<?php

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->get('/', '\App\Iota\Http\Controllers\ImportController@get');
