<?php

class ShoeEuTest extends TestCase
{
    /**
     * @var \App\Iota\Sort\ShoeEu
     */
    protected $shoeEu;

    public function setUp()
    {
        parent::setUp();
        $this->shoeEu = app('App\Iota\Sort\ShoeEu');
    }

    public function test_sort()
    {
        $items = [
            [1, 'ABC', 'aa', '11', 'SHOE_EU',],
            [2, 'ABC', 'aa', '9', 'SHOE_EU',],
            [2, 'ABC', 'aa', '10', 'SHOE_EU',],
        ];

        $expected = [
            [2, 'ABC', 'aa', '9', 'SHOE_EU',],
            [2, 'ABC', 'aa', '10', 'SHOE_EU',],
            [1, 'ABC', 'aa', '11', 'SHOE_EU',],
        ];

        $this->assertEquals(collect($expected), $this->shoeEu->sort(collect($items)));
    }
}
