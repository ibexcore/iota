<?php

class ShoeUkTeste extends TestCase
{
    /**
     * @var \App\Iota\Sort\ShoeUk
     */
    protected $shoeUk;

    public function setUp()
    {
        parent::setUp();
        $this->shoeUk = app('App\Iota\Sort\ShoeUk');
    }

    public function test_sort()
    {
        $items = [
            [1, 'ABC', 'aa', '11', 'SHOE_UK',],
            [2, 'ABC', 'aa', '9', 'SHOE_UK',],
            [2, 'ABC', 'aa', '10', 'SHOE_UK',],
        ];

        $expected = [
            [2, 'ABC', 'aa', '9', 'SHOE_UK',],
            [2, 'ABC', 'aa', '10', 'SHOE_UK',],
            [1, 'ABC', 'aa', '11', 'SHOE_UK',],
        ];

        //dd($this->shoeUk->sort(collect($items)));

        $this->assertEquals(json_encode($expected), json_encode($this->shoeUk->sort(collect($items))));
    }

    public function test_sort_with_children()
    {
        $items = [
            [1, 'ABC', 'aa', '11', 'SHOE_UK',],
            [2, 'ABC', 'aa', '9', 'SHOE_UK',],
            [3, 'ABC', 'aa', '9 (child)', 'SHOE_UK',],
            [4, 'ABC', 'aa', '10', 'SHOE_UK',],
            [5, 'ABC', 'aa', '8 (child)', 'SHOE_UK',],
        ];

        $expected = [
            [5, 'ABC', 'aa', '8 (child)', 'SHOE_UK',],
            [3, 'ABC', 'aa', '9 (child)', 'SHOE_UK',],
            [2, 'ABC', 'aa', '9', 'SHOE_UK',],
            [4, 'ABC', 'aa', '10', 'SHOE_UK',],
            [1, 'ABC', 'aa', '11', 'SHOE_UK',],
        ];

        $this->assertEquals(collect($expected), $this->shoeUk->sort(collect($items)));
    }

    public function test_sort_with_different_children()
    {
        $items = [
            [1, 'ABC', 'aa', '1', 'SHOE_UK',],
            [2, 'ABC', 'aa', '11 (child)', 'SHOE_UK',],
            [3, 'ABC', 'aa', '4.5 (child)', 'SHOE_UK',],
            [4, 'ABC', 'aa', '9 (child)', 'SHOE_UK',],
        ];

        $expected = [
            [3, 'ABC', 'aa', '4.5 (child)', 'SHOE_UK',],
            [4, 'ABC', 'aa', '9 (child)', 'SHOE_UK',],
            [2, 'ABC', 'aa', '11 (child)', 'SHOE_UK',],
            [1, 'ABC', 'aa', '1', 'SHOE_UK',],
        ];

        $this->assertEquals(collect($expected), $this->shoeUk->sort(collect($items)));
    }

    public function test_sort_with_children_key_capitalised()
    {
        $items = [
            [1, 'ABC', 'aa', '1', 'SHOE_UK',],
            [2, 'ABC', 'aa', '11 (Child)', 'SHOE_UK',],
            [3, 'ABC', 'aa', '4.5 (Child)', 'SHOE_UK',],
            [4, 'ABC', 'aa', '9 (Child)', 'SHOE_UK',],
        ];

        $expected = [
            [3, 'ABC', 'aa', '4.5 (Child)', 'SHOE_UK',],
            [4, 'ABC', 'aa', '9 (Child)', 'SHOE_UK',],
            [2, 'ABC', 'aa', '11 (Child)', 'SHOE_UK',],
            [1, 'ABC', 'aa', '1', 'SHOE_UK',],
        ];

        $this->assertEquals(collect($expected), $this->shoeUk->sort(collect($items)));
    }
}
