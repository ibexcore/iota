<?php

class ClothingShortTest extends TestCase
{
    /**
     * @var \App\Iota\Sort\ClothingShort
     */
    protected $clothing;

    public function setUp()
    {
        parent::setUp();
        $this->clothing = app('App\Iota\Sort\ClothingShort');
    }

    public function test_sort()
    {
        $items = [
            [1, 'ABC', 'aa', 'L', 'CLOTHING_SHORT',],
            [2, 'ABC', 'aa', 'S', 'CLOTHING_SHORT',],
            [2, 'ABC', 'aa', 'M', 'CLOTHING_SHORT',],
            [2, 'ABC', 'aa', 'XL', 'CLOTHING_SHORT',],
        ];

        $expected = [
            [2, 'ABC', 'aa', 'S', 'CLOTHING_SHORT',],
            [2, 'ABC', 'aa', 'M', 'CLOTHING_SHORT',],
            [1, 'ABC', 'aa', 'L', 'CLOTHING_SHORT',],
            [2, 'ABC', 'aa', 'XL', 'CLOTHING_SHORT',],
        ];

        $this->assertEquals(collect($expected), $this->clothing->sort(collect($items)));
    }
}
