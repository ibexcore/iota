<?php

class CsvTest extends TestCase
{
    /**
     * @var \App\Iota\Csv
     */
    protected $csv;

    public function setUp()
    {
        parent::setUp();
        $this->csv = app(\App\Iota\Csv::class);
    }

    /**
     * @expectedException  \App\Iota\Exceptions\FileNotFoundException
     */
    public function test_file_not_found()
    {
        $this->csv->toArray('/some/bad/file');
    }

    public function test_to_array()
    {
        $file = dirname(__FILE__) . '/stubs/csv.csv';

        $result = $this->csv->toArray($file);

        $this->assertEquals(3, count($result));

        $this->assertEquals(['id', 'description', 'username', 'quantity'], $result[0]);
        $this->assertEquals(['1', 'Eldon Base for stackable storage shelf, platinum', 'Muhammed MacIntyre', '3'], $result[1]);
    }

    public function test_to_array_without_headers()
    {
        $file = dirname(__FILE__) . '/stubs/csv.csv';

        $result = $this->csv->toArray($file, true);

        $this->assertEquals(2, count($result));

        $this->assertEquals(['1', 'Eldon Base for stackable storage shelf, platinum', 'Muhammed MacIntyre', '3'], $result[0]);
        $this->assertEquals(['2', '1.7 Cubic Foot Compact "Cube" Office Refrigerators', 'Barry French', '293'], $result[1]);
    }
}
