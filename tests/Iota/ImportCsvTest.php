<?php

class ImportCsvTest extends TestCase
{
    /**
     * @var \App\Iota\Csv|\Mockery\Mock
     */
    protected $csv;

    /**
     * @var \App\Iota\ImportCsv
     */
    protected $importCsv;

    public function setUp()
    {
        parent::setUp();
        $this->csv = Mockery::mock(\App\Iota\Csv::class);
        $this->importCsv = app(\App\Iota\ImportCsv::class);
        //$this->importCsv->setCsv($this->csv);
    }

    public function test_import()
    {
        $file = __DIR__ . '/stubs/products.csv';
        $result = $this->importCsv->import($file);

        $this->assertEquals(2, count($result));
        $keyed = collect($result)->keyBy('PLU');

        $abc = [
            'PLU' => 'ABC',
            'name' => 'Vision Windrush',
            'sizes' => [
                ['SKU' => '103', 'size' => '9 (child)'],
                ['SKU' => '101', 'size' => '9'],
                ['SKU' => '100', 'size' => '11'],
                ['SKU' => '109', 'size' => '12'],
            ]
        ];

        $def = [
            'PLU' => 'DEF',
            'name' => 'Vision Jacket',
            'sizes' => [
                ['SKU' => '200', 'size' => 'S'],
                ['SKU' => '209', 'size' => 'M'],
                ['SKU' => '201', 'size' => 'L'],
            ]
        ];
        $this->assertEquals(json_encode($abc), json_encode($keyed['ABC']));
        $this->assertEquals(json_encode($def), json_encode($keyed['DEF']));
    }
}
