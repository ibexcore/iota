<?php

namespace App\Iota;

class ImportCsv
{
    protected $csv;

    protected $sort;

    public function __construct(Csv $csv, Sort $sort)
    {
        $this->csv = $csv;
        $this->sort = $sort;
    }

    public function setCsv(Csv $csv)
    {
        $this->csv = $csv;
        return $this;
    }

    public function import(string $file)
    {
        $data = $this->csv->toArray($file);

        $data = collect($data)
            // there is some data with whitespaces, we need to clean this
            ->transform(function ($value) {
                foreach ($value as $key => &$item) {
                    $item = trim($item);
                }
                return $value;
            })
            // now, group them by PLU
            ->groupBy(1)
            // sort them accordingly
            ->transform(function ($value) {
                $a =  $this->sort->sort($value[0][4], $value);
                return $a;
            });

        $data = $data->transform(function ($value) {
            return [
                'PLU' => $value[0][1],
                'name' => $value[0][2],
                'sizes' => $value->transform(function ($value) {
                    return ['SKU' => $value[0], 'size' => $value[3]];
                })
            ];
        });

        return $data;
    }
}
