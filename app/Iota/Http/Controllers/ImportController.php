<?php

namespace App\Iota\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Iota\ImportCsv;

class ImportController extends Controller
{
    protected $import;

    public function __construct(ImportCsv $import)
    {
        $this->import = $import;
    }

    public function get()
    {
        return $this->import->import(base_path('public/products.csv'));
    }
}
