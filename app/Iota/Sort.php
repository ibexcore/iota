<?php

namespace App\Iota;

use App\Iota\Sort\Sorting;
use Illuminate\Support\Collection;

class Sort
{
    public function sort(string $by, Collection $items)
    {
        return $this->makeSorter($by)->sort($items);
    }

    protected function makeSorter(string $name): Sorting
    {
        return app(__NAMESPACE__ . '\Sort\\' . studly_case(strtolower($name)));
    }
}
