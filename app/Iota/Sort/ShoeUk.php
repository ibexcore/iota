<?php

namespace App\Iota\Sort;

use Illuminate\Support\Collection;

class ShoeUk extends Sort implements Sorting
{
    public function sort(Collection $items): Collection
    {
        $items->transform(function ($value) {
            $sorting = $value[3];
            if (strpos(strtolower($sorting), 'child') !== false) {
                $sorting = intval($sorting) / 100;
            }
            $value['tmp_sorting'] = floatval($sorting);
            return $value;
        });

        return $items->sort(function ($a, $b) {
            return $a['tmp_sorting'] <=> $b['tmp_sorting'];
        })
            // remove the tmp_sorting key
            ->transform(function ($value) {
                unset($value['tmp_sorting']);
                return $value;
            })->values();
    }
}
