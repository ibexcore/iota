<?php

namespace App\Iota\Sort;

use Illuminate\Support\Collection;

class ClothingShort implements Sorting
{
    /**
     * This has to be in order of smallest => largest as it relies on the array keys for the sort order
     *
     * @var array
     */
    protected $sizes = ['xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl', 'xxxxl'];

    public function sort(Collection $items): Collection
    {
        $this->sizes = array_flip($this->sizes);
        $items->transform(function ($value) {
            // could probably do with some sanity checking here...
            $value['tmp_sorting'] = $this->sizes[strtolower($value[3])];
            return $value;
        });

        $items = $items->sort(function ($a, $b) {
            return $a['tmp_sorting'] - $b['tmp_sorting'];
        });

        return $items->transform(function ($value) {
            unset($value['tmp_sorting']) ;
            return $value;
        })->values();
    }
}
