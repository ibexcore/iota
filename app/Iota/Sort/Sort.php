<?php

namespace App\Iota\Sort;

use Illuminate\Support\Collection;

abstract class Sort
{
    /**
     * @param \Illuminate\Support\Collection $items
     * @return \Illuminate\Support\Collection
     */
    public function sort(Collection $items): Collection
    {
        return $items->sort(function ($a, $b) {
            return $a[3] - $b[3];
        })->values();
    }
}
