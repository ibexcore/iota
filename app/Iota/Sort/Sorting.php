<?php

namespace App\Iota\Sort;

use Illuminate\Support\Collection;

interface Sorting
{
    /**
     * Sort the items
     *
     * @param \Illuminate\Support\Collection $items
     * @return \Illuminate\Support\Collection
     */
    public function sort(Collection $items): Collection;
}
