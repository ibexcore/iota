<?php

namespace App\Iota;

use App\Iota\Exceptions\FileNotFoundException;

class Csv
{
    /**
     * Import a CSV file to an array
     *
     * @param string $file
     * @param bool $withoutHeaders
     * @return array
     * @throws \App\Iota\Exceptions\FileNotFoundException
     */
    public function toArray(string $file, bool $withoutHeaders = false)
    {
        if (!file_exists($file)) {
            throw new FileNotFoundException("File {$file} was not found");
        }

        $result = array_map('str_getcsv', (file($file)));
        if ($withoutHeaders) {
            // pop the first element off the array, removing the headers
            array_shift($result);
        }
        return $result;
    }
}
