# IOTA test

I've kept everything contained within 2 folders, for sake of simplicity at `/app/Iota` and `/tests/Iota`.
It isn't the most efficient program, as there is multiple loops and could easily be improved by reducing the amount of loops.
But for the sake of development speed and code simplicity, I've left them in.
The program is a bit rough around the edges, but it does the job.

There is a typo with the csv, it's called `CLOTHING_SHORT` instead of `CLOTHING_SORT`. I've left the CSV as-is and created
classes around that name within the CSV.

The main work is in `ImportCsv` class. If this was not "throw-away" code or was to be used more than once, then I'd suggest
making this a console command, where you could assign columns to how they should be treated.

I've used Lumen for convenience sake, as it already had DI setup and the really helpful `Collection` class. If this is
an issue, I can easily rewrite it in "vanilla" PHP.

## Deployment

Clone the repo

`git clone https://ibexcore@bitbucket.org/ibexcore/iota.git`

Install dependencies

`composer install`

Run tests

`vendor/bin/phpunit`

To get the JSON, go to the index.php with a web browser. For example, I have a subdomain `iota.local.ibex.com` that points
to `/public/index.php`
